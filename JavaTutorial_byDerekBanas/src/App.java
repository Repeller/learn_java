import java.util.*;
import java.util.stream.IntStream;

public class App {

    // this will get inputs from the keyboard
    static Scanner sc = new Scanner(System.in);

    //final is like a constant
    final double shortPi = 3.14159;

    public static void main(String[] args) throws Exception {

        // like console write line
        System.out.println("Hello, World!");
        
        int var1 = 100;
        int v2, v3;

        v2 = var1 * 2;
        v3 = var1 * 3;

        System.out.println("var1: " + var1 + " v2: " + v2 + " v3: " + v3);

        boolean happy = true;

        char a = 'a';
        // \n - new line
        // \t - tabs
        // \b - backspace
        // \f form feets
        // \r - returns
        // \" \' \\

        // float - only has 6 points of precision
        float fNum = 1.1111111111111111F;
        float fNum2 = 1.1111111111111111F;

        System.out.println("Float : " +
                (fNum + fNum2));

        // double - has 15 points of precision
        double thousand = 1e+3;
        System.out.println(thousand);

        // long - really big numbers
        long bigNum = 123_456_789;
        System.out.println(bigNum);

        // part 2 - 10:05 *************** casting **************
        System.out.println("part 2 - 10:05 *************** casting **************");

        // convert int to long
        int smInt = 10;
        long smLong = smInt;
        System.out.println(smLong);

        double cDbl = 1.234;
        // here we set the wanted type, that we want "cDbl" to be converted to (int)
        int cInt = (int) cDbl;
        System.out.println(cInt);

        long bigLong = 2147486470L;
        int bInt = (int) bigLong;
        System.out.println(bInt);

        // convert double to string (all the simple types have a toString method)
        String favNum = Double.toString(1.518);
        System.out.println(favNum);

        //Byte.parseByte()
        int strInt = Integer.parseInt("10");
        System.out.println(strInt);

        // part 3 - 12:58 *************** math functions **************
        System.out.println("part 3 - 12:58 *************** math functions **************");

        System.out.println("5+5 = " + (5+5));
        System.out.println("5-3 = " + (5-3));
        System.out.println("5*5 = " + (5*5));
        System.out.println("5/5 = " + (5/5));
        System.out.println("5%4 = " + (5%4));
        System.out.println("5/4 = " + (5.0/4.0));

        // incMe++ is the same as IncMe + 1
        int incMe = 0;
        // if you place the ++ after the value, it outputs the value and adds to it after.
        System.out.println("incMe : " + (incMe++));
        // if you place the ++ before the value, it adds to the value, before it prints the value
        System.out.println("incMe : " + (++incMe));
        // this will give us a value of 2

        // adds 10 to the number
        incMe += 10;

        // random number
        int minNum = 5;
        int maxNum = 20;
        int randNum = minNum + (int)(Math.random() * ( (maxNum - minNum) + 1) );
        System.out.println("randNum : " + randNum);

        // part 4 - 17:05 *************** strings **************
        String name = "alex";
        String wName = name + "ziegler";
        wName += ", is my name";
        String drsDog = "K" + 9;

        System.out.println(wName.charAt(0));
        System.out.println(wName.contains("alex"));
        System.out.println(wName.indexOf("alex"));
        System.out.println(wName.length());

        // never use == for comparing strings
        // use .equals
        System.out.println("dog equals cat : " + ("dog".equals("cat")));
        System.out.println("dog equals cat : " + ("dog".equalsIgnoreCase("cat")));

        // you could also use wName.compareToIgnoreCase
        // -1, 0, 1,
        System.out.println(name.compareTo("alex123"));

        String str1 = "String method tutorial";
        String str2 = "compareTo method example";
        String str3 = "String method tutorial";

        int cT_var1 = str1.compareTo( str2 );
        System.out.println("str1 & str2 comparison: "+cT_var1);

        int cT_var2 = str1.compareTo( str3 );
        System.out.println("str1 & str3 comparison: "+cT_var2);

        int cT_var3 = str2.compareTo("compareTo method example");
        System.out.println("str2 & string argument comparison: "+cT_var3);


    }
}
